package com.zzuli.desk.extractResume;

public class inform {
	private  String name;
	private  String sex;
	private	 String birthday;
	private  String politics_status;
	private  String graduation_time;
	private  String graduation_school;
	private  String major;
	private  String graduation_time_T;
	private  String graduation_school_T;
	private  String major_T;
	private String  inf;
	public String getInf() {
		return inf;
	}
	public void setInf(String inf) {
		this.inf = inf;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPolitics_status() {
		return politics_status;
	}
	public void setPolitics_status(String politics_status) {
		this.politics_status = politics_status;
	}
	public String getGraduation_time() {
		return graduation_time;
	}
	public void setGraduation_time(String graduation_time) {
		this.graduation_time = graduation_time;
	}
	public String getGraduation_school() {
		return graduation_school;
	}
	public void setGraduation_school(String graduation_school) {
		this.graduation_school = graduation_school;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getGraduation_time_T() {
		return graduation_time_T;
	}
	public void setGraduation_time_T(String graduation_time_T) {
		this.graduation_time_T = graduation_time_T;
	}
	public String getGraduation_school_T() {
		return graduation_school_T;
	}
	public void setGraduation_school_T(String graduation_school_T) {
		this.graduation_school_T = graduation_school_T;
	}
	public String getMajor_T() {
		return major_T;
	}
	public void setMajor_T(String major_T) {
		this.major_T = major_T;
	}
	

}
