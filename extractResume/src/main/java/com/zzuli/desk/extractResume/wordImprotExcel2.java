package com.zzuli.desk.extractResume;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableIterator;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
@SuppressWarnings("unused")
public class wordImprotExcel2 {
	public static void main(String[] args) throws IOException {
		wordImprotExcel2 w = new wordImprotExcel2("C:\\报名表","C:\\imp");
		List<inform> infornList = w.getInformList();
		try {
			w.downLown(infornList,"汇总表.xls","表格式错误.txt","学历经历两条以上.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	private String sourceFlieDirPath; // 原文件夹路径
	private String exportFileDirPath; // 导出文件夹路径
	private StringBuffer bf;	//记录学习经历大于两条的word文件的名称
	private StringBuffer error;//记录非.docx文件名称
	private static String[] HEADLINE = { "序号", "专业方向", "姓名", "性别", "出生年月", "政治面貌", "毕业时间", "毕业院校", "专业", "毕业时间", "毕业院校",
			"专业", "备注" };
	
	//private List<String[]> infoList;
	
	public StringBuffer getBf() {
		return bf;
	}

	public void setBf(StringBuffer bf) {
		this.bf = bf;
	}

	public StringBuffer getError() {
		return error;
	}

	public void setError(StringBuffer error) {
		this.error = error;
	}



	public wordImprotExcel2(String sourceFlieDirPath ,String exportFileDirPath) {
		super();
		this.sourceFlieDirPath = sourceFlieDirPath;
		this.exportFileDirPath = exportFileDirPath;
		bf = new StringBuffer();
		error = new StringBuffer();
	}
	
	
	/***
	 * 
	 * @param list 				所有word文件中的提取信息
	 * @param fileName          生产的xml表的名称
	 * @param errorFileName     生成解析错误的txt文件的名称
	 * @param tempFileName		生产学习经历大于两条的txt文件的名称
	 * @throws IOException
	 */
	public void downLown(List<inform> list,String fileName,String errorFileName ,String tempFileName) throws IOException {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("汇总表");
		for (int i = 0; i < 13; i++) {
			sheet.setColumnWidth(i, 6500);//设置列长度
		}
		//标题字样
		HSSFFont font = workbook.createFont();
		font.setFontName("宋体");
		font.setFontHeightInPoints((short) 14);// 设置字体大小
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 粗体显示
		

		
		//内容字样
		HSSFFont font2 = workbook.createFont();
		font2.setFontName("宋体");
		font2.setFontHeightInPoints((short) 11);
		
		// 设置标题样式
		HSSFCellStyle cellStyle = workbook.createCellStyle();// 获取一个样式
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 设置居中
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); // 下边框
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);// 左边框
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);// 上边框
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);// 右边框
		cellStyle.setFont(font);
		
		// 设置标题样式
		HSSFCellStyle cellStyle1 = workbook.createCellStyle();// 获取一个样式
		cellStyle1.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 设置左对齐
		cellStyle1.setBorderBottom(HSSFCellStyle.BORDER_THIN); // 下边框
		cellStyle1.setBorderLeft(HSSFCellStyle.BORDER_THIN);// 左边框
		cellStyle1.setBorderTop(HSSFCellStyle.BORDER_THIN);// 上边框
		cellStyle1.setBorderRight(HSSFCellStyle.BORDER_THIN);// 右边框
		cellStyle1.setFont(font);
				
		//设置内容样式
		HSSFCellStyle cellStyle2 = workbook.createCellStyle();
		cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
		cellStyle2.setBorderBottom(HSSFCellStyle.BORDER_THIN); // 下边框
		cellStyle2.setBorderLeft(HSSFCellStyle.BORDER_THIN);// 左边框
		cellStyle2.setBorderTop(HSSFCellStyle.BORDER_THIN);// 上边框
		cellStyle2.setBorderRight(HSSFCellStyle.BORDER_THIN);// 右边框
		cellStyle2.setFont(font2);

		
		
		//创建第一行
		HSSFRow title = sheet.createRow(0);
		//创建第一行第一列单元格
		HSSFCell titleN = title.createCell(0);
		//对单元格设置属性
		titleN.setCellStyle(cellStyle);
		//对单元格设置内容
		titleN.setCellValue("2018年公开招聘人事代理人员汇总表");
		//对其他列设置单元格样式，以保证合并完后样式一致
		for(int i=1;i<13;i++){
			HSSFCell othertitle = title.createCell(i);
			othertitle.setCellStyle(cellStyle);
		}
		
		
		//创建第二行第一列单元格，并是个样式内容
		HSSFRow title1 = sheet.createRow(1);
		HSSFCell titleN1 = title1.createCell(0);
		titleN1.setCellStyle(cellStyle1);
		titleN1.setCellValue("部门：");
		for(int i=1;i<13;i++){
			HSSFCell othertitle = title1.createCell(i);
			othertitle.setCellStyle(cellStyle1);
		}
		

		//合并第一行中第一列到到第十三列单元格
		CellRangeAddress cra = new CellRangeAddress(0, 0, 0, 12);
		// 在sheet里增加合并单元格
		sheet.addMergedRegion(cra);
		
		//合并第二行中第一列到到第十三列单元格
		CellRangeAddress cra1 = new CellRangeAddress(1, 1, 0, 12);
		// 在sheet里增加合并单元格
		sheet.addMergedRegion(cra1);
		
		
		//设置第三行第一列到第三列单元格样式、内容
		HSSFRow headline = sheet.createRow(2);
		for (int j = 0; j < 13; j++) {
			HSSFCell cell = headline.createCell(j);
			cell.setCellStyle(cellStyle2);
			cell.setCellValue(HEADLINE[j]);
		}
		
		
		int rowl = 3;//标记下次需要创建的行标
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) != null) {
				//创建行
				HSSFRow row = sheet.createRow(rowl);
				//创建列单元格并对应填充样式和值
				HSSFCell cel0 = row.createCell(0);
				cel0.setCellStyle(cellStyle2);
				cel0.setCellValue(rowl - 2);

				HSSFCell celll = row.createCell(1);
				celll.setCellStyle(cellStyle2);
				celll.setCellValue("");

				HSSFCell cell = row.createCell(2);
				cell.setCellStyle(cellStyle2);
				cell.setCellValue(list.get(i).getName());

				HSSFCell cel1 = row.createCell(3);
				cel1.setCellStyle(cellStyle2);
				cel1.setCellValue(list.get(i).getSex());

				HSSFCell cel2 = row.createCell(4);
				cel2.setCellStyle(cellStyle2);
				cel2.setCellValue(list.get(i).getBirthday());

				HSSFCell cel3 = row.createCell(5);
				cel3.setCellStyle(cellStyle2);
				cel3.setCellValue(list.get(i).getPolitics_status());

				HSSFCell cel4 = row.createCell(6);
				cel4.setCellStyle(cellStyle2);
				cel4.setCellValue(list.get(i).getGraduation_time());

				HSSFCell cel5 = row.createCell(7);
				cel5.setCellStyle(cellStyle2);
				cel5.setCellValue(list.get(i).getGraduation_school());

				HSSFCell cel6 = row.createCell(8);
				cel6.setCellStyle(cellStyle2);
				cel6.setCellValue(list.get(i).getMajor());

				HSSFCell cel7 = row.createCell(9);
				cel7.setCellStyle(cellStyle2);
				cel7.setCellValue(list.get(i).getGraduation_time_T());

				HSSFCell cel8 = row.createCell(10);
				cel8.setCellStyle(cellStyle2);
				cel8.setCellValue(list.get(i).getGraduation_school_T());

				HSSFCell cel9 = row.createCell(11);
				cel9.setCellStyle(cellStyle2);
				cel9.setCellValue(list.get(i).getMajor_T());

				HSSFCell cel10 = row.createCell(12);
				cel10.setCellStyle(cellStyle2);
				cel10.setCellValue(list.get(i).getInf());
				rowl++;
			}
		}

		FileOutputStream fout;
		try {
			File  fi =  new File(exportFileDirPath);
			if(!fi.exists()){
				fi.mkdirs();
			}
			
			File  f= new File(exportFileDirPath +"\\"+ fileName);
			if(!f.exists()){
				f.createNewFile();
			}
			fout = new FileOutputStream(f);
			workbook.write(fout);
			
			//讲解析错误的Word文件名称生成文件
			IOUtils.write(this.getError().toString(), new FileOutputStream(exportFileDirPath +"\\"+errorFileName), "gbk");
			//讲习经历大于两条的Word文件名称生成文件
			IOUtils.write(this.getBf().toString(), new FileOutputStream(exportFileDirPath +"\\"+tempFileName), "gbk");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			workbook.close();
		}

	}

	/***
	 * 获取解析所有word 格式的信息集合
	 * @return
	 * @throws IOException 
	 */
	public List<inform> getInformList() throws IOException {
		File f = new File(sourceFlieDirPath);
		if(!f.exists()){
			f.mkdirs();
		}
		File[] fa = f.listFiles();
		List<inform> in = new ArrayList<inform>();
		for (int i = 0; i < fa.length; i++) {
			//提取每一个word表中的信息
			inform inf = null;
			System.out.println(fa[i].getName().substring(fa[i].getName().length()-2,fa[i].getName().length()));
			if(fa[i].getName().substring(fa[i].getName().length()-2,fa[i].getName().length()).equals("oc")){
				inf = initLocalExtractorDOC(sourceFlieDirPath + "\\" + fa[i].getName());
			}else if(!fa[i].getName().substring(fa[i].getName().length()-2,fa[i].getName().length()).equals("oc")){
				inf = initLocalExtractorDOXC(sourceFlieDirPath + "\\" + fa[i].getName());
			}
			
			/*if(fa[i].getName().contains(".pdf")){
				inf = initLocalExtractorPDF(sourceFlieDirPath + "\\" + fa[i].getName());
			}*/
			if(inf!=null){
				in.add(inf);
			}
			
		}
		return in;
	}

	private inform initLocalExtractorPDF(String filepath) {
			/***
			 * 之前也没有解析过pdf ， 自己解决的过程。
			 * 百度解析poi 解析pdf，（https://blog.csdn.net/weixin_36571568/article/details/78419525）这个参考
			 * 看到：
			 * 		POI解析pdf文档内容：
					注意三个import：
					import org.apache.pdfbox.pdmodel.PDDocument;
					import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
					import org.apache.pdfbox.text.PDFTextStripper;
				知道org.apache.pdfbox这个中jar包导入。
				百度其对应的maven依赖。(http://maven.outofmemory.cn/org.apache.pdfbox/pdfbox/1.8.8/)
				加入pom文件，并更新项目
			 */
			
			PDDocument document = null ;
		try {
			 document=PDDocument.load(filepath);
			 /***
			  * 查看完PDDocument API后，发现有一个静态方法
			  * static PDDocument	| load(File file) 解析PDF。
			  *  创建完PDDocument对象， 然后并没有找到获取表格的对象。
			  * 
			  * 然后百度。 org.apache.pdfbox。（https://pdfbox.apache.org/docs/2.0.2/javadocs/overview-summary.html）
			  * 
			  * 找到了，org.apache.pdfbox.pdmodel.graphics.form	此包处理存储在PDF文档中的表单XObject。(注：谷歌浏览器，自带翻译)
			  * 
			  */
			 
			 PDFTextStripper stripper = new PDFTextStripper();
			 System.out.println(stripper.getText(document).toString());
			 document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/***
	 * 解析doc文件
	 * @param filepath
	 * @return
	 */
	private inform initLocalExtractorDOC(String filepath) {
		/**
		 * 之前没有解析过doc的文件。 自己实现的过程。
		 * 给大家些参考，我个人解决问题的思路。
		 * 
		 * 百度下，找到需要org.apache.poi.hwpf.HWPFDocument，解析doc文件
		 * 百度API(http://poi.apache.org/apidocs/org/apache/poi/hwpf/HWPFDocument.html)
		 * 发现	getRange()：返回覆盖整个文档的范围，但不包括任何页眉和页脚。
		 */
		FileInputStream inputStream = null;
		HWPFDocument msWord = null;
		POIFSFileSystem pfs = null;
		try {
			inputStream = new FileInputStream(filepath);
			pfs = new POIFSFileSystem(inputStream);
			msWord = new HWPFDocument(pfs);
			     Range range = msWord.getRange();//得到文档的读取范围 
			     /***
			      * 百度查看org.apache.poi.hwpf.usermodel.Range API文档(http://poi.apache.org/apidocs/org/apache/poi/hwpf/usermodel/Range.html)
			      * 然后发现获取Table 的方法只有这一个：getTable(Paragraph paragraph)
			      * 传的值是Paragraph。 猜测内部是一行一行解析的。 表格插入在哪一行不确定。有多少表格也不确定。
			      * 猜测八成有一个获取Table集合的方法。
			      * 在空白处输入Table,按住art+/,只看org.apache.poi.hwpf... 这样的包，找到了一个org.apache.poi.hwpf.usermodel.TableIterator 
			      * 百度API(http://poi.apache.org/apidocs/org/apache/poi/hwpf/usermodel/TableIterator.html)，找到了其初始化用法，且是一个迭代器
			      * 安迭代器的方法处理即可
			      */
			
			     TableIterator it =  new TableIterator(range);//获取所有Table
			    
		     		   
			     //我的需求中只有一个table， 所以这里我只获取一个了。多个可以循环。
			     it.hasNext();
			     Table  tb = (Table) it.next(); 
			     inform inf = new inform();
			     
			     
			     inf.setInf(filepath);
			     //下面的内容和解析.doxc,差不过， 不过这里获取文本不是getText()方法，而是text()方法
			     inf.setName(tb.getRow(0).getCell(1).text());  //获取word文件表中姓名，并放到inform中。

					inf.setSex(tb.getRow(0).getCell(3).text());//性别

					inf.setBirthday(tb.getRow(0).getCell(5).text());//出生年月

					inf.setPolitics_status(tb.getRow(3).getCell(1).text());//政治面貌

					String time = tb.getRow(7).getCell(1).text();

					StringBuffer sbtime = new StringBuffer("");
					//String splitString[] = time.trim().split("");
					for (String str : time.trim().split("")) {
						if (!str.equals(" ")) {
							sbtime.append(str);
						}
					}
					inf.setGraduation_time(sbtime.toString());//毕业时间

					inf.setGraduation_school(tb.getRow(7).getCell(2).text());//毕业学校

					inf.setMajor(tb.getRow(7).getCell(3).text());//专业

					String time_T = tb.getRow(8).getCell(1).text();
					sbtime.setLength(0);
					for (String str : time_T.trim().split("")) {
						if (!str.equals(" ")) {
							sbtime.append(str);
						}
					}
					inf.setGraduation_time_T(sbtime.toString());//毕业时间

					inf.setGraduation_school_T(tb.getRow(8).getCell(2).text());//毕业学校

					inf.setMajor_T(tb.getRow(8).getCell(3).text());//专业

					String timetemp = tb.getRow(9).getCell(1).text();
					if (Pattern.compile("^[-\\+]?[\\d]*$").matcher(timetemp.substring(0, 1)).matches()) {
						bf.append(filepath.substring(7)+"\r\n");
					}
					return inf;	   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error.append(filepath);
			error.append("\r\n");
		}finally{
			if(inputStream!=null){
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();		
				}
			}
		}
		return null;
	}

	/*
	 * 加载本地的一个Word文件，并进行解析
	 * 
	 */
	public inform initLocalExtractorDOXC(String filepath) throws IOException {
		FileInputStream inputStream = null;
		XWPFDocument msWord = null;
		try {
			inputStream = new FileInputStream(filepath);
			msWord = new XWPFDocument(inputStream);
			List<XWPFTable> tablelist = msWord.getTables();

			XWPFTable tb = tablelist.get(0);//因为知道每个表中只有一个tabla，这里就只获取第一个了。没有遍历获取。
			// 迭代行，从第2行开始

			inform inf = new inform();
			inf.setInf(filepath);
			inf.setName(tb.getRow(0).getCell(1).getText());  //获取word文件表中姓名，并放到inform中。

			inf.setSex(tb.getRow(0).getCell(3).getText());//性别

			inf.setBirthday(tb.getRow(0).getCell(5).getText());//出生年月

			inf.setPolitics_status(tb.getRow(3).getCell(1).getText());//政治面貌

			String time = tb.getRow(7).getCell(1).getText();

			StringBuffer sbtime = new StringBuffer("");
			//String splitString[] = time.trim().split("");
			for (String str : time.trim().split("")) {
				if (!str.equals(" ")) {
					sbtime.append(str);
				}
			}
			inf.setGraduation_time(sbtime.toString());//毕业时间

			inf.setGraduation_school(tb.getRow(7).getCell(2).getText());//毕业学校

			inf.setMajor(tb.getRow(7).getCell(3).getText());//专业

			String time_T = tb.getRow(8).getCell(1).getText();
			sbtime.setLength(0);
			for (String str : time_T.trim().split("")) {
				if (!str.equals(" ")) {
					sbtime.append(str);
				}
			}
			inf.setGraduation_time_T(sbtime.toString());//毕业时间

			inf.setGraduation_school_T(tb.getRow(8).getCell(2).getText());//毕业学校

			inf.setMajor_T(tb.getRow(8).getCell(3).getText());//专业

			String timetemp = tb.getRow(9).getCell(1).getText();
			if (Pattern.compile("^[-\\+]?[\\d]*$").matcher(timetemp.substring(0, 1)).matches()) {
				bf.append(filepath.substring(7)+"\r\n");
			}
			return inf;
		} catch (Exception e) {
			e.printStackTrace();//对异常进行了处理，就不打印了。
			error.append(filepath);
			error.append("\r\n");
		} finally {
			try {
				if(msWord!=null){
					msWord.close();
				}
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
